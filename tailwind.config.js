module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx,html}'],
  media: false,
  theme: {
    extend: {
      screens: {
        landscape: { raw: '(orientation: landscape)' },
        xs: { max: '425px' },
      },
    },
  },
  safelist: [
    {
      pattern: /bottom-([1-8])/,
    },
  ],
  variants: {
    extend: {},
  },
  plugins: [
    require('daisyui'),
    require('@rvxlab/tailwind-plugin-ios-full-height'),
  ],
  daisyui: {
    themes: [
      {
        agentmate: {
          primary: '#b42020',
          'primary-focus': '#c52323',
          'primary-content': '#ffffff',

          secondary: '#f4d85a',
          'secondary-focus': '#f7e38a',
          'secondary-content': '#ffffff',

          accent: '#96dbd7',
          'accent-focus': '#57b5a8',
          'accent-content': '#ffffff',

          neutral: '#202020',
          'neutral-focus': '#303030',
          'neutral-content': '#ffffff',

          'base-100': '#ffffff',
          'base-200': '#f9fafb',
          'base-300': '#d1d5db',
          'base-content': '#1f2937',

          info: '#2094f3',
          success: '#199759',
          warning: '#ff9900',
          error: '#f22510',
        },
      },
      'light',
    ],
  },
}
