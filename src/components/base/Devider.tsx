export const Devider = () => (
  <div className="bg-neutral w-full px-6 md:px-20 lg:px-28">
    <div className="bg-primary h-1 w-full lg:w-3/4 xl:w-2/3 mx-auto"></div>
  </div>
)
