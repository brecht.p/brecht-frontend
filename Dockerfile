FROM node:16.13.1-alpine3.12 AS builder

WORKDIR /app

COPY ./package.json /app/package.json
COPY ./.npmrc /app/.npmrc

RUN yarn

COPY ./public /app/public
COPY ./src /app/src
COPY ./craco.config.js /app/craco.config.js
COPY ./.prettierrc /app/.prettierrc
COPY ./.eslintrc /app/.eslintrc
COPY ./tailwind.config.js /app/tailwind.config.js
COPY ./tsconfig.json /app/tsconfig.json
COPY ./tsconfig.eslint.json /app/tsconfig.eslint.json

FROM nginx:1.21.0

COPY ./nginx/default.template /nginx.template
RUN envsubst < /nginx.template > /etc/nginx/conf.d/default.conf
COPY ./nginx/nginx-entrypoint.sh /nginx-entrypoint.sh
COPY --from=builder /app/build /usr/share/nginx/html

EXPOSE 80

ENTRYPOINT [ "bash", "/nginx-entrypoint.sh" ]
